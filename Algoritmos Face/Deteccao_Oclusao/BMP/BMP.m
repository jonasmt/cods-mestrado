function BMP()
clear;clc;
pegar_imagens();
end



function pegar_imagens()
destino  = 'C:\Users\jonas\Dropbox\USP\AR Faces com Viola Jones';
cd (destino);
nomes = load('names_ims_ocluidas.mat');
nomes = nomes.met;
cd ('AR faces_128_128');

img_comparacao = imread('M-001-01.bmp');
img_comparacao = rgb2gray(img_comparacao);
img_comparacao = im2double(img_comparacao);

for i = 1: size(nomes,2);
    imagem = imread(char(nomes{i}));
    im_gray = im2double(rgb2gray(imagem));
    deteccao_blocos(img_comparacao,im_gray);
    pause(.5);
end
end

function deteccao_blocos(imTr, imTeste)
dim = size(imTeste,2); %Pega a dimens�o da imagem
dim_mascara = 4; % J� que a imagem � 128x128 � interessante colocar uma mascara m�ltipla de 2,4,8,16,32 e 64.

for i=1:dim_mascara:dim 
    for k=1:dim_mascara:dim
        mascaraTr = imTr(i:i+dim_mascara-1,k:k+dim_mascara-1); % fun��o para pegar a m�scara da imagem de teste
        mascaraTs = imTeste(i:i+dim_mascara-1,k:k+dim_mascara-1); % fun��o para pegar a m�scara da imagem de treinamento
        erro = (mascaraTr - mascaraTs).^2;
        erro = sum(sum(erro));
        if (erro > 1) %se a diferen�a entre os blocos for maior que 400 coloco a oclus�o nos blocos.
           imTeste(i:i+dim_mascara-1,k:k+dim_mascara-1) = 0;
        end       
    end
end
%Vai em cada imagem 
imshowpair(imTr,imTeste,'montage');
pause(0.3);
end
