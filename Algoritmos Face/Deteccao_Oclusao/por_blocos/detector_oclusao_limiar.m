function conjunto = detector_oclusao_limiar(conjunto)
[dim1, dim2] = size(conjunto);
    conjunto = uint8(conjunto);
    for i=1:dim1
        for k =1: dim2
           if conjunto(i,k)< 70
               conjunto(i,k) = 0;
           end
        end
    end
end