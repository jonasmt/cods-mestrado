function crop_image(imagem, qtdeBlocos)  % passar uma quantidade de blocos que tenha raiz quadrada
dim  = size(imagem,1); %S� dar certo se as imagens forem quadradas
divisao = sqrt(qtdeBlocos);
intervalo = dim/divisao

aux = [];
cont = 1;
for i =1: intervalo : dim
    
    for j =1: intervalo : dim
        subplot(divisao,divisao,cont);
        imshow(uint8(imagem(i:i+intervalo-1,j:j+intervalo-1)),[]);
        cont = cont+1;
        aux = [aux;imagem(i:i+intervalo-1,j:j+intervalo-1)];
        
    end
    
end

%imshow(uint8(aux));

end