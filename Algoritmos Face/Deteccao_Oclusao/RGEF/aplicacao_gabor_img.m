function im_Res = aplicacao_gabor_img(imagem,mascara)
dim = size(imagem,2); %Pega a dimens�o da imagem
dim_mascara = size(mascara,1); % J� que a imagem � 128x128 � interessante colocar uma mascara m�ltipla de 2,4,8,16,32 e 64.

intervalo = round(dim_mascara/2); %Intervalo respons�vel por delinear a nova matriz, a partir de onde a mesma come�a
i = 0;

for lin= intervalo: dim-intervalo 
    i = i+1;
    j=0;
    for col =intervalo:dim - intervalo
        j = j + 1;
        valor = imagem(lin-intervalo+1:lin+intervalo-1 , col-intervalo+1:col+intervalo-1);
        im_Res(i,j) = sum(sum(mascara.*valor));
    end
end

% figure(1)
% imshow(uint8(imagem));
% figure(2)
%imshow(uint8(im_Res));

end