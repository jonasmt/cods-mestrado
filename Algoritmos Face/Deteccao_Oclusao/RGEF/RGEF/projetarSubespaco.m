function [subespacos,imsMedias] = projetarSubespaco(conjunto)
u = 8;
disp('Gerando Subespa�os');
for i =1: u %Vou em cada espa�o!
    disp(i);
    [U,imMedia] = calcPca(conjunto{i});%segundo campo � imMedia
    subespacos{i} = U;
    imsMedias{i} = imMedia;
    ver_projecao(U)
end
end

function ver_projecao(U)
[lin,col] = size(U);
dim = sqrt(lin);

for i=1:col
   
   eigenface = U(:,i); 
   eigenface = reshape(eigenface,[dim dim]);
   imshow(eigenface,[]);
   title('eigenface');
   pause(0.3);
   
end
end