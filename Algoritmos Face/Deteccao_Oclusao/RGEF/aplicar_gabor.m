origem = pwd;
mascara = gabor_fn(1,90,4,1,1); 
%gb=gabor_fn(sigma,theta,lambda,psi,gamma)
% cd '..\..\Baseados em Subespašo\Funcoes_Extras'; 
[treinamento,teste] = leituraArquivo(1,'false');
[lin,col] = size(teste);
qtdeBlocos = 9;
% cd(origem);
for i = 1: col
    imagem = teste(:,i);
    imagem = reshape(imagem,[sqrt(lin) sqrt(lin)]);
    aplicacao_gabor_img(imagem,mascara);
    %imshow(uint8(imagem));
    crop_image(imagem,qtdeBlocos)
    pause();
end


