function RGEF2()
clear; close all; clc;
% db = 1;iluminacao = 'sem';
% cd ('../../Baseados em Subespa�o/Funcoes_Extras');
[treinamento,teste] = leituraArquivo(1,'false'); %lendo minha base

[~,col] = size(treinamento);

for i = 1: col
    imagem = teste(:,i);
    im = converter(imagem);
    filtroGabor(im);
    impixelinfo;
    pause();
end
end

function filtroGabor(imagem)

imagem=im2double(imagem);
aux = imagem;
%Gabor filter size 7x7 and orientation 90 degree
%gamma=0.5; %raio da elipse 
%psi=0; %phase (deslocamento da fase);
%theta=90; %orienta��o
%bw=2.8; %bandwidth or effective width (sigma - desvio padr�o do envelope %gaussiano)
lambda=2; % wavelength (comprimento da onda)
%pi = 180;
qtde_orientacoes = 8;

vet = [];
vet = [vet,aux];
for u =1: qtde_orientacoes
    theta = (u*pi)/8;
    gb = gabor(imagem,theta,lambda,pi);
    vet = [vet,gb];
    
end

imshow(vet);
end

function gb = gabor(imagem,theta,lambda,pi)
dim = size(imagem,1);
for x=1:dim
    for y=1:dim
        x_theta=imagem(x,y)*cos(theta)+imagem(x,y)*sin(theta);
        y_theta=-imagem(x,y)*sin(theta)+imagem(x,y)*cos(theta);
%         gb(x,y)= exp(-(x_theta.^2/2*bw^2+ gamma^2*y_theta.^2/2*bw^2))*cos(2*pi/lambda*x_theta+psi);
        gb(x,y)= exp(-(x_theta.^2+ y_theta.^2))*cos(2*pi/lambda*x_theta);
    end
end

end

function imagem = converter(imagem)
lin = sqrt(size(imagem,1));
imagem = uint8(reshape(imagem,[lin lin]));
end