function RGEF()
destino  = 'C:\Users\GrIA\Dropbox\USP\AR Faces com Viola Jones';
cd (destino);
nomes = load('names_ims_ocluidas.mat');
nomes = nomes.met;
cd ('AR faces_128_128');


for i = 1: size(nomes,2);
    imagem = imread(char(nomes{i}));
    imagem= rgb2gray(imagem);
    filtroGabor(imagem);
    pause(.8);
end




end

function filtroGabor(imagem)
dim = size(imagem,1);
imagem=im2double(imagem);


% aux = imagem;
%Gabor filter size 7x7 and orientation 90 degree

vet= [];
% vet = [vet,imagem];
teta = [15,30,45,5]
for i = 1: size(teta,2)
    theta=teta(i); %orientação
    %bw=2.8; %bandwidth or effective width
    lambda=1; % wavelength (frequência)
    pi=1;
    for x=1:dim
        for y=1:dim
            x_theta=imagem(x,y)*cos(theta)+imagem(x,y)*sin(theta);
            y_theta=-imagem(x,y)*sin(theta)+imagem(x,y)*cos(theta);
            %         gb(x,y)= exp(-(x_theta.^2/2*bw^2+ gamma^2*y_theta.^2/2*bw^2))*cos(2*pi/lambda*x_theta+psi);
            gb(x,y)= exp(-(x_theta.^2+ y_theta.^2))*cos(2*pi/lambda*x_theta);
            
        end
    end
    origem = pwd;
    cd ('C:\Users\GrIA\Desktop');
    imwrite(gb,strcat(num2str(i+4),'.png'));
    cd(origem);
    vet = [vet,gb];
    if i ==3
        
        
    end
end
imshow(vet);
pause();
%gamma=0.5; %aspect ratio
%psi=0; %phase



% figure(2);
% imshow(gb);
% title('filtered image');
end