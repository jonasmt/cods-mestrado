function [U,imMedia] = projetarSubespaco(conjunto)
%u = 8;
% cd(origem);
% cd(destino);
disp('Gerando Subespa�os');
%for i =1: u %Vou em cada espa�o!
 %   disp(i);
    [U,imMedia] = calcPca(conjunto);%segundo campo � imMedia
%     subespacos{i} = U;
%     imsMedias{i} = imMedia;
%      ver_projecao(U)
% end
%cd(origem);
end

function ver_projecao(U)
[lin,col] = size(U);
dim = sqrt(lin);

for i=1:col
   
   eigenface = U(:,i); 
   eigenface = reshape(eigenface,[dim dim]);
   imshow(eigenface,[]);
   title('eigenface');
   pause();
   
end
end