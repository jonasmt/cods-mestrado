function reconstrucao(imMedia, U,teste,partes)
disp('reconstruindo');
qtdImagens = size(teste,2);

for i=1: qtdImagens %imagens de teste
    imagem_ocluida = teste(:,i);
    [l,~] = size(imagem_ocluida);
    dim = sqrt(l);
    
    imOcluidas = imagem_ocluida - imMedia;
    w = U'*imOcluidas;
    reconstrucao = U*w;
    
    
    imReconstruida = imMedia + reconstrucao;
    imagem_ocluida = reshape(imagem_ocluida,[dim dim]);
    imReconstruida = reshape(imReconstruida,[dim dim]);
    
    
    %figure(1);
    imshowpair(imagem_ocluida,imReconstruida,'montage');
    pause();
    imagem_ocluida = double(imagem_ocluida);
    imReconstruida = double(imReconstruida);
    int = 2;
    partes_ocluidas = crop_image(imagem_ocluida, partes,int); % Recebendo os k peda�os da imagem oclu�da
    %p1 = size(partes1)
    %pause();
    int = 3;
    
    partes_reconstruidas = crop_image(imReconstruida, partes,int); %Recebendo os k peda�os da imagem reformada
    %p2 = size(partes2)
    %pause();
    construir_Mascara_Oclusao(partes_ocluidas,partes_reconstruidas);
end

end


function construir_Mascara_Oclusao(partes_ocluidas,partes_reconstruidas)
[~,c] = size(partes_ocluidas); %Vou s� pegar o n�mero de c�lulas
%como ele preenche por linhas, vou pegar a raiz de c para ir preenchendo de
% maneira mais vis�vel.
aux = sqrt(c);

im = {};
cont = 1;
for i = 1: aux
    
    for j=1: aux
        dim = size(partes_ocluidas{cont},2);
        p_ocluida = partes_ocluidas{cont};
        p_reconst = partes_reconstruidas{cont};
        
        matriz_resultante = ((p_ocluida-p_reconst)*100)./(p_ocluida);
        pr = sum(matriz_resultante(:));
        pr = round(pr);
        
        if pr < -2800
            partes_ocluidas{cont} = zeros(dim,dim);
            
        end
        im{cont} = partes_ocluidas{cont};   
       
        
        %imshowpair(uint8(partes_ocluidas{cont}),uint8(p_reconst),'montage');
        %pause();
        %disp(pr);
        %pause();
        
        
  
        cont = cont+1;
    end
    
end

iterador = 1;
imagem = [];
cont = 1;

for i =1:aux
    vet = [];
    for k =1:aux
       
       
       vet = [vet,im{cont}]; 
        
       cont = cont+1;
    end
    
    if iterador ==1
        imagem = [imagem,vet];
        
    else
        imagem = [imagem;vet];
    end
    
    iterador = iterador+1;
    
end

imshow(uint8(imagem));

pause(0.5);

end


