function conjunto = aplicarGabor(treinamento)
u = 8; pi = 180;
disp('Aplicando Gabor');
[lin,col] = size(treinamento);

for i = 1: col
    imagem = treinamento(:,i);
    imagem = reshape(imagem,[sqrt(lin) sqrt(lin)]);
    
    for k=1: u
        orientacao = (k*pi)/8;
        mascara = gabor_fn(3,orientacao,10,1,1); %gb=gabor_fn(sigma,theta,lambda,psi,gamma)
        
        im = aplicacao_gabor_img(imagem,mascara);
        
%         imshow(im,[]);
%         pause(0.2);
        
        if i ==1
            conjunto{k} = im(:);
        else
            conjunto{k} = [conjunto{k},im(:)];
        end   
    end
end


end