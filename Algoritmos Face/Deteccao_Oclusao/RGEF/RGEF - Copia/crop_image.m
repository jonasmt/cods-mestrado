function aux = crop_image(imagem, qtdeBlocos,int)  % passar uma quantidade de blocos que tenha raiz quadrada
dim  = size(imagem,1); %S� dar certo se as imagens forem quadradas
divisao = sqrt(qtdeBlocos);
intervalo = dim/divisao;

aux = {};

cont = 1;
%figure(int);
for i =1: intervalo : dim
    
    for j =1: intervalo : dim
        
        %subplot(divisao,divisao,cont);
        im = imagem(i:i+intervalo-1,j:j+intervalo-1);
        %imshow(uint8(im),[]);
        
        %pause(0.2);

        aux{cont} = im;
        cont = cont+1;
    end
    
end

%imshow(uint8(aux));

end