function skinColor()
%cd 'C:\Users\jonas\Dropbox\USP\Qualificação\Base AR\Base Completa\AR Face - JPG_ 256\dbf1_JPG';
cd 'C:\Users\jonas\Google Drive\Universidades\Base de Dados - Yale - AR\BASE AR\AR - 256x256\AR Face - JPG\dbf1_JPG';
a = dir('*.jpg');
b = {a.name};

[~,c] = size(b);

for i = 1: c
    k = char(b(i))
    I=imread(k);
    I=double(I);
    [hue,s,v]=rgb2hsv(I);
    %subplot(2,2,1);imtool(hue);subplot(2,2,2);imtool(s);subplot(2,2,3);imtool(v);
    
    cb =  0.148* I(:,:,1) - 0.291* I(:,:,2) + 0.439 * I(:,:,3) + 128;
    cr =  0.439 * I(:,:,1) - 0.368 * I(:,:,2) -0.071 * I(:,:,3) + 128;
    [w h]=size(I(:,:,1));
    
    for i=1:w
        for j=1:h
            if  140<=cr(i,j) && cr(i,j)<=165 && 140<=cb(i,j) && cb(i,j)<=195 && 0.01<=hue(i,j) && hue(i,j)<=0.1
                segment(i,j)=0;
            else
                segment(i,j)=1;
            end
        end
    end
    im(:,:,1)=I(:,:,1).*segment;
    im(:,:,2)=I(:,:,2).*segment;
    im(:,:,3)=I(:,:,3).*segment;
    im = uint8(im);
    figure,imshow(im);
    pause(1);
    close all;
    
    
end
