function skin()
%cd 'C:\Users\jonas\Dropbox\USP\Qualifica��o\Base AR\Base Completa\AR Face - JPG_ 256\dbf1_JPG'
cd 'C:\Users\jonas\Google Drive\Universidades\Base de Dados - Yale - AR\BASE AR\AR - 256x256\AR Face - JPG\dbf1_JPG';
a = dir('*.jpg'); % Recebe todo o diret�rio
b = {a.name}; %b = nome de todos os diret�rios da pasta.

[~,col] = size(b);
origem = pwd;
cont = 1;
vet = [];
vet2 = [];
for k=1:col 
    cd (origem);
    aux = char(b(k)); %converte para String o arquivo
    I=imread(aux); % Leio a imagem 
    gray = rgb2gray(I); %Coloco ela na escala de cinza para depois recolorir o preto e branco
    gray = double(gray); %Coloco a escala de cinza em double para poder somar com imagem bw processada
    [l,c,~] = size(I); %fa�o isso para pegar pixel a pixel da imagem
    if(size(I, 3) > 1)
        bw = zeros(l, c);
        for i = 1:l
            for j = 1:c
                R = I(i,j,1); %Pego o valor vermelho do pixel
                G = I(i,j,2); %Pego o valor verde do pixel
                B = I(i,j,3); %pego o valor azul do pixel
                
                if(R > 95 && G > 40 && B > 20) %Aqui analisa os cabelos ou outras partes pretas
                    v = [R,G,B]; %Coloca cada pixel em um vetor para comparar eles
                    if((max(v) - min(v)) > 25) % Aqui detecta as bordas definindo a pele da pessoa
                        if(abs(R-G) > 15 && R > G && R > B) % Analisa as cores R,G e B onde o B � o menor de todos na imagem
                   
                            bw(i,j) = 1; %coloco a parte da pele como branco
                        end
                    end
                end
            end
        end
    end
    
    %imshow(im); %Pode-se exibir a imagem preto e branco
     
    bw = gray.*bw; %Multiplico ponto a ponto com a finalidade de onde tiver um multiplicar pelo seu sequente na escala de cina
    bw = uint8(bw); %Converto para exibir na escala de cinza em 8 bits, ou seja 256 cores
    bw = imresize(bw,[128 128]);
    %cd ('C:\Users\jonas\Desktop\imgsOcluidas');
    aux = bw == 0;
    bw(aux) = 255;
    if cont <= 4
        vet = [vet,bw];
    %imshow(bw);
    elseif (cont >4) && (cont <=8)
        vet2 = [vet2,bw];
    else
        whos;
        pause();
        d = [vet;vet2];
        imshow(d);
        pause();
    end
    pause(0.2);
    cont = cont+1;
    %imwrite(bw,strcat(int2str(k),'.png'));
    k
  
end

end
