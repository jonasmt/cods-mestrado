%function images = viola_jones()
origem = 'C:\Users\jonas\Google Drive\Bases Ar_Sdumla\Yale';
destino = 'C:\Users\jonas\Google Drive\Bases Ar_Sdumla\Yale - Cropped';
cd(origem);
images = dir('*.pgm');
imagens = {images.name};

for i = 1:size(imagens, 2)
    cd(origem);
    i
    nome =  imagens{i};
    
    im = imread(nome);
   
    detector = vision.CascadeObjectDetector;
    bbox = step(detector, im);
    
    out = insertObjectAnnotation(im, 'rectangle', bbox, 'detection');
    
    n_im = imcrop(im, bbox);
    im2 = imresize(n_im,[256 256]);
    
    cd(destino);
    imwrite(im2,nome);
    
end
% end
