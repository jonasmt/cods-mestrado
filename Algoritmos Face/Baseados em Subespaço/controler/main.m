function main()
clear;clc;origem = pwd;db = 1;
[treinamento,teste,nomes] = leituraDeDados(db,origem);

metodos = {'PCA','APCA','GPCA'};
variancia = 90; %matriz de vari�ncia

for v=1: size(variancia,2)
    cd (origem);
    cd ('..\Reconstrucoes\AR');
    dir_current = strcat('VAR',num2str(variancia(v)));
    mkdir(dir_current);
    cd (dir_current);
    dir_current = pwd;
    
    for i=1: size(metodos,2)
        tecnica = metodos{i};
        cd(origem);
        reconstrucao(treinamento,teste,nomes,db,origem,tecnica,i,dir_current,variancia(v));
    end
    
end

end

function reconstrucao(treinamento,teste,nomes,db,origem,nome_tecnica,num_tec,dir_current,variancia)

particoes = [5,10,20,30,40,50];
pastaStorage = '';
for i= 1: size(particoes,2)
    
    disp(sprintf('Particao: %d |Variancia: %d | tecnica: %s',i,variancia,nome_tecnica));
    
    cd (origem);
    [tr,ts,name]= particionar_conjunto(treinamento,teste,nomes,particoes(i),db);
    
    
    for k = 1: size(tr,2)
        
        destRec = dir_current;
        
        if k == 1
            %cd ..;
            cd (destRec);
            if i ==1
                mkdir(nome_tecnica);
            end
            cd (nome_tecnica);
            pastaStorage = strcat(nome_tecnica,num2str(particoes(i)));
            mkdir(pastaStorage);
            cd (pastaStorage);
            pastaStorage = pwd;
            
        end
        executarAlgoritmos(origem,tr,ts,name,k,pastaStorage,num_tec,variancia)
        
    end
end

end


function executarAlgoritmos(origem,tr,ts,name,k,pastaStorage,num_tec,variancia)
variancia = variancia*0.01;

switch num_tec
    case 1 %PCA
        cd (origem);
        cd ..;
        cd ('PCA');
        PCA2(tr{k},ts{k},name{k},pastaStorage,variancia);
        
    case 2 %APCA
        cd (origem);
        cd ..;
        cd ('APCA');
        APCA2(tr{k},ts{k},name{k},pastaStorage,variancia);
        
    case 3 %GPCA
        cd (origem);
        cd ..;
        cd ('GPCA');
        GPCA2(tr{k},ts{k},name{k},pastaStorage,variancia);
        
end

end

