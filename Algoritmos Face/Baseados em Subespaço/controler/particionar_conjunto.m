function [tr,ts,name] = particionar_conjunto(treinamento,teste,nomes,particoes,db)

[~,col] = size(treinamento);
[~,col2] = size(teste);
if db == 1
    individuos = 100;
end

intervaloTr = col/individuos*particoes;
auxTr = intervaloTr;
intervaloTs = col2/individuos*particoes;
auxTs = intervaloTs;
iniTr = 1;
iniTs = 1;

tr = {};
ts = {};
name = {};

for i =1: individuos/particoes
    
    tr{i} = treinamento(:,iniTr:intervaloTr);
    iniTr = intervaloTr+1;
    intervaloTr = intervaloTr + auxTr;
    
    ts{i} = teste(:,iniTs:intervaloTs);
    name{i} = nomes(iniTs:intervaloTs); %colocando a matriz de nomes;
    iniTs = intervaloTs+1;
    intervaloTs = intervaloTs + auxTs;
    
end

end
