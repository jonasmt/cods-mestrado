
function reconstrucao(imsMedias, subespacos,teste)
disp('reconstruindo');
qtdeEspacos = size(subespacos,2);
qtdImagens = size(teste{1},2);

disp(qtdImagens);
for i=1: qtdImagens %imagens de teste
   
   for k=1: qtdeEspacos %vai em cada espa�o
       
       teste2 = teste{k};
       [l,~] = size(teste2);
       dim = sqrt(l);
       
       imagem = teste2(:,i);
       
       
       U = subespacos{k}; 
       imMedia = imsMedias{k};
       imOcluidas = imagem - imMedia;
       w = U'*imOcluidas;
       reconstrucao = U*w;
      
       imReconstruida = imMedia + reconstrucao;
       imagemAux = reshape(imagem,[dim dim]);

       imReconstruidaAux = reshape(imReconstruida,[dim dim]);
       imshowpair(imagemAux,imReconstruidaAux,'montage');
       title(strcat('subespa�o: ',num2str(k)));
       pause();
   end
end

end