function reconstrucao(imMedia,erro_geral, erro_medio_amostra,erro_medio_geral, imagem,set_num_amostrados,E)

[H,vet_erro,vet_posicao,yrs]=criarMatrizH(erro_geral, erro_medio_amostra,erro_medio_geral, imagem,set_num_amostrados,E);

kpixels = [2048];

n = 2;

for it = 1:4
    %intervalo = kpixels(it);
    %     w = abs(yrs - imagem);
    %     desvio = std(w);
    %     teta = n*desvio;
    %
    %     aux = w < teta;
    %
    %     vetor = [imagem,H,yrs];
    intervalo = 2048;
    
    [~,pos] = sort(vet_erro); %Pego os pixels remanescentes com menor erro
    pos = pos(1:intervalo);
    pos = vet_posicao(pos);
    H(pos) = imagem(pos);
    
    yrs = imMedia + reconstruir_face(E,H);
    vetor = [imagem,H,yrs];
    
    ver_projecao(vetor,it);
    pause(0.5);
  
end
end


