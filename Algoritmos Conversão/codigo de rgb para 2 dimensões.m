clear;close all; clc;
cd ('C:\Users\jonas\Desktop\imgs');
warning off;

imgs = dir('*.bmp');
imgs = {imgs.name};

vet = [];
aux = [];
cont = 1;
for i=1 : length(imgs)
    im = imread(imgs{i});
%     im = reshape(im,[128 128 3]);
    
    im = reshape(im, [256 256 3]); %// Reshape so that it's a 3D matrix - Note that this is column major
    Ifinal = flipdim(im,2); % // The clever transpose
    f = imresize(Ifinal,[256 256]);
    vet = [vet;f];
%     imshow(vet);
%     pause();
    
    if (cont == 13)
        aux= [aux,vet];
        vet = [];
        cont = 0;
        
    end
    
    
    cont = cont + 1;
end
imshow(aux);
cd ..;
imwrite(aux,'matriz.png');
