%qtde_instancias = 800;
%qtde_classes = 100;
%gerar_rotulos_inteiros(qtde_instancias,qtde_classes)
function rotulos = gerar_rotulos_inteiros(qtde_instancias,qtde_classes)
intervalo = qtde_instancias/qtde_classes;

rotulos =[];
for i =1: qtde_classes
    
    for k=1: intervalo
        rotulos = [rotulos,i] ;
    end
    
end
rotulos = rotulos';
end